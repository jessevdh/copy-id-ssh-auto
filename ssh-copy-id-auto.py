#!/usr/bin/python3

import os
import os.path
import getpass
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-L', '--list', action='store', help="list of ip's or hostname. 1 per line.", required=True)

args = parser.parse_args()

# check if file exists, is readable or not empty
if os.path.isfile(args.list) and os.path.getsize(args.list) > 0 and os.access(args.list, os.R_OK):
    ips = [i for i in open(args.list, 'r').readlines()]
    user = input("Username: ")
    passwd = getpass.getpass('Password: ')

    for ip in ips:
        cmd = 'ssh-copy-id {0}@{1}'.format(user, ip)
        sshpass_cmd = 'sshpass -p {0} {1}'.format(passwd, cmd)
        os.system(sshpass_cmd)
        print("Key added: ", ip)   # prints if successful
else:
    print("I'm sorry but %s is either empty, does not exist or is not readable" % (args.list))
    quit()
